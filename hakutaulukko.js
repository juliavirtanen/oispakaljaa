/*  ESIMERKKI-BAARITAULUKKO TESTAAMISTA VARTEN
    var baari1 = {
    nimi: "Kallionhovi",
    osoite: "Kuja 3",
    aukioloaika: "La-Su 15-01",
    tuotteet: {"Kalja": 3.5,
        "Lonkero": 4,
        "Viini": 6}

};

var baari2 = {
    nimi: "Las Palmas",
    osoite: "Vaasankatu 6",
    aukioloaika: "Ma-Su 14-04",
    tuotteet: {"Kalja": 4.5,
        "Lonkero": 3,
        "Viini": 7}
};

var baarit = [baari1, baari2];
*/

// Luo etusivun tulos-divin sisään elementtejä ja tulostaa hakutulokset oikeisiin paikkoihin
function printtaaTulos(baarit){ // ottaa parametrinä baari-oliotaulukon, joka on käännetty jsonstringistä pääohjelmassa
    for (var i=0; i < baarit.length; i++) {

        var h3 = document.createElement("h3");
        h3.innerHTML = baarit[i].Nimi;
        document.getElementById('results').appendChild(h3);

        var lista1 = document.createElement("li");
        lista1.innerHTML = baarit[i].Osoite;
        document.getElementById('results').appendChild(lista1);

        var lista2 = document.createElement("li");
        lista2.innerHTML = baarit[i].Aukioloaika;
        document.getElementById('results').appendChild(lista2);

        var lista3 = document.createElement("li");
        lista3.innerHTML = "Tuotteet";
        document.getElementById('results').appendChild(lista3);


        var listaviini = document.createElement("li");
        listaviini.className ="luokka";
        listaviini.innerHTML = "Viini: " + baarit[i].Viini + "€";

        var listasiideri = document.createElement("li");
        listasiideri.className = "luokka";
        listasiideri.innerHTML = "Siideri: " + baarit[i].Siideri + "€";

        var listalonkero = document.createElement("li");
        listalonkero.className = "luokka";
        listalonkero.innerHTML = "Lonkero: " + baarit[i].Lonkero + "€";

        var listashotti = document.createElement("li");
        listashotti.className = "luokka";
        listashotti.innerHTML = "Shotti: " + baarit[i].Shotti + "€";

        var listakalja = document.createElement("li");
        listakalja.className = "luokka";
        listakalja.innerHTML = "Kalja: " + baarit[i].Kalja + "€";


        document.getElementById('results').appendChild(listakalja);
        document.getElementById('results').appendChild(listaviini);
        document.getElementById('results').appendChild(listasiideri);
        document.getElementById('results').appendChild(listalonkero);
        document.getElementById('results').appendChild(listashotti);



        /* Tää on paskana :(
        var tuotenimi = Object.getOwnPropertyNames(baarit[i].tuotteet);
        for (var e = 0; e < tuotenimi.length; e++) {
            var tuote1 = document.createElement("ol");
            tuote1.innerHTML = tuotenimi[e] + ": " + baarit[i].tuotteet[tuotenimi[e]] + "€";
            document.getElementById('results').appendChild(tuote1);
        }
        */
    }



    // var tuote1 = document.createElement("ol");
   // tuote1.innerHTML = baarit[i].tuotteet.Kalja;
   // var tuote2 = document.createElement("ol");
   // tuote2.innerHTML = baarit[i].tuotteet.Lonkero;
   // var tuote3 = document.createElement("ol");
   // tuote3.innerHTML = baarit[i].tuotteet.Viini;
    //document.getElementById('results').appendChild(tuote2);
    //document.getElementById('results').appendChild(tuote3);



}



