DROP DATABASE IF EXISTS Oispakaljaa;
CREATE DATABASE Oispakaljaa;
USE Oispakaljaa;

CREATE TABLE Baari
(
  Id INT NOT NULL,
  Nimi VARCHAR(40) NOT NULL,
  Osoite VARCHAR(100)  NOT NULL,
  Alue INT NOT NULL,
  Aukioloaika varchar(40),
  Happyhour INT,
  PRIMARY KEY (Id)
);

CREATE TABLE Juomavalikoima
(
  Kalja float NOT NULL,
  Viini float NOT NULL,
  Siideri float NOT NULL,
  Lonkero float NOT NULL,
  Shotti float NOT NULL,
  Id INT NOT NULL,
  FOREIGN KEY (Id) REFERENCES Baari(Id)
);

CREATE TABLE Varustelu
(
  Bilis BOOLEAN NOT NULL,
  Keittio BOOLEAN NOT NULL,
  Lautapelit BOOLEAN NOT NULL,
  Jukeboxi BOOLEAN NOT NULL,
  Karaoke BOOLEAN,
  Id INT NOT NULL,
  FOREIGN KEY (Id) REFERENCES Baari(Id)
);

INSERT INTO Baari VALUES (1, "Bar Kalliohovi", "Vaasankatu 17", 4, "ma-to 14-00, pe 14-04, la 10-04, su 10-02", null);
INSERT INTO Baari VALUES (2, "Iltakoulu", "Vaasankatu 5", 4, "ma-to 14-02, pe 14-04, la 10-04, su 14-02", null);
INSERT INTO Baari VALUES (3, "Solmu Pub", "Vaasankatu 8", 4, "ma 15-01, ti-to 15-02, pe-la 15-03, su 15-01", null);
INSERT INTO Baari VALUES (4, "Olutravintola Hilpeä Hauki", "Vaasankatu 7", 4, "joka päivä 12-02", null);
INSERT INTO Baari VALUES (5, "Kustaa Vaasa", "Vaasankatu 10", 4, "ma-to 16-02, pe-la 14-02, su 16-02", "arkisin 16-18", null);
INSERT INTO Baari VALUES (6, "Pub Heinähattu", "Vaasankatu 23", 4,  "ma-pe 16-02, la-su 14-02", null);
INSERT INTO Baari VALUES (7, "Bar Molotow", "Vaasankatu 29", 4,  "ma-to 16-02, pe-la 14-03, su 16-02", null);
INSERT INTO Baari VALUES (8, "Kultapalmu", "Vaasankatu 8", 4, null);
INSERT INTO Baari VALUES (9, "Siima Baari", "Vaasankatu 25", 4, "ma-su 09-02", null);
INSERT INTO Baari VALUES (10, "On The Rocks", "Porvoonkatu 1", 4, " ma-to 10.30-02, pe 10.30-04, la 14-04, su 14-02", null);

INSERT INTO Baari VALUES (11, "Kaiku", "Kaikukatu 4", 3, "ke-to 22-04, pe-la 22-05", null);
INSERT INTO Baari VALUES (12, "Musta Kissa", "Toinen linja 15", 2, "ti-su 17-02", null);
INSERT INTO Baari VALUES (13, "Kipinä", "Kolmas linja 34", 2, "ma-pe 12-02, la 11-02, su 12-02", null);
INSERT INTO Baari VALUES (14, "Majava Baari", "Porthaninkatu 9", 2, "ma-su 12-02", null);
INSERT INTO Baari VALUES (16, "Roskapankki", "Helsinginkatu 20", 1, "ma-su 09-02", null);
INSERT INTO Baari VALUES (17, "Om'pu", "Siltasaarenkatu 15", 2, "ma-to 09-02, pe-la 09-03, su 09-02", null);
INSERT INTO Baari VALUES (18, "Toveri", "Castréninkatu 3", 2, "ma -to 17-24, pe-la 17-02", null);
INSERT INTO Baari VALUES (19, "Bar Loosister", 2, "Hämeentie 50", "ma-to 14- 00, ke-to 14- 02, pe-la 12-05, su 12-02", null );
INSERT INTO Baari VALUES (20, "Siltanen", "Hämeentie 13 B", 3,  "ma-to 11-02, pe-la 11-03", null);

INSERT INTO Baari VALUES (21, "Mäkikupla", "Torkkelinkatu 2 B", 3,  "ma-pe 11-02, la-su 13-02", null);
INSERT INTO Baari VALUES (22, "Tenkka", "Helsinginkatu 15", 1,  "ma-to 10-02, pe 10-2.30, la 11-2.30, su 11-02", null);
INSERT INTO Baari VALUES (23, "Sirdie", "Kolmas linja 21", 2, "ma-su 16-02", null);
INSERT INTO Baari VALUES (24, "5th Street Bar & Cafe", "Viides linja 7", 2,  "ma-ti 14-01, ke-la 14-02, su 14-02", null);
INSERT INTO Baari VALUES (25, "Pub Porthan", "Porthaninkatu 10", 3,  "ma-su 16-02", null);
INSERT INTO Baari VALUES (26, "Wino", "Fleminginkatu 11", 3,  " ma-to 17-00, pe-la 17-02", null);
INSERT INTO Baari VALUES (27, "Sivukirjasto", "Fleminginkatu 5", 3,  "ma-to 14-02, pe-su 12-02", null);
INSERT INTO Baari VALUES (28, "Bar Lepakkomies", "Helsinginkatu 1", 1,  "ma-to 12-02, pe 12-04, la 10-04, su 10-02", null);
INSERT INTO Baari VALUES (29, "Pulmu", "Fleminginkatu 13", 3,  "ma-to 18-02, pe-la 18-4.30, su 18-02", null);
INSERT INTO Baari VALUES (30, "Silmu Pub", "Hämeentie 30", 3,  "ma-su 15-02", null);

INSERT INTO Baari VALUES (31, "Bar Bronco", "Hämeentie 23", 3,  "ma-pe 14-02, la-su 12-02", null);
INSERT INTO Baari VALUES (32, "Kolme Kaisaa", "Hämeentie 29", 3,  "ma-ti 11-24, ke-la 11-4.30, su 11-24", null);
INSERT INTO Baari VALUES (34, "Rytmi", "Toinen linja 2", 2,  "ma-la 11-02", null);
INSERT INTO Baari VALUES (35, "Loung3", "Kolmas linja 18", 2,  "ma-pe 11-23, la 15-23", null);
INSERT INTO Baari VALUES (36, "Cafe Talo", "Hämeentie 2", 2,  "ma-ti 15-24, ke-to 15-01, pe 15-02, la 12-02, su 15-23", null);
INSERT INTO Baari VALUES (37, "Bar 21", "Helsinginkatu 21", 1, "ma-su 09-02", null);
INSERT INTO Baari VALUES (38, "Arizona", "Castréninkatu 7", 2,  "ma-su 09-02", null) ;
INSERT INTO Baari VALUES (39, "Stadin Tähti", "Mäkenlänkatu 2", 4,  "ma-pe 11-05, la 14-05, su 22-05", null);
INSERT INTO Baari VALUES (40, "Cella", "Fleminginkatu 15", 3,  "ma-pe 15-02, la-su 12-02", null);

INSERT INTO Baari VALUES (41, "Relaxin", "Helsinginkatu 13", 1,  "ma-su 12-02", null) ;
INSERT INTO Baari VALUES (42, "Kallion pörssi", "Alppikatu 17", 3,  "ma-ti 12-00, ke-to 12-02, pe 12-02, la 10-02", null);
INSERT INTO Baari VALUES (43, "Pressapari", "Wallininkatu 10",2,  "ma-su 10-02", null);
INSERT INTO Baari VALUES (44, "Kurvitar", "Hämeentie 58-60", 1,  "ma-to 09-02, pe-la 9-4.30, su 09-02", null);
INSERT INTO Baari VALUES (45, "Extreme bar", "Porthaninkatu 9", 2,  "ma-pe 18-02, la-su 17-02", null);
INSERT INTO Baari VALUES (46, "Kallion Seurahuone", "Fleminginkatu 6", 3,  "ma-to 13-01, pe 18-02, la 13-02, su 12-00", null);
INSERT INTO Baari VALUES (47, "Populus", "Aleksis Kiven katu 22", 4, "joka päivä ympäri vuorokauden",null);
INSERT INTO Baari VALUES (48, "Kallion Mestari", "Kustaankatu 4 A", 4,  "ma-su 09-02", null);
INSERT INTO Baari VALUES (50, "Ravintola Oiva", "Porthaninkatu 5", 2, "ma 10.30-16, ti 10.30-24, ke-to 10.30–01, pe 10.30-04, la 10-04, su 10-17", "arkisin 15-18");

INSERT INTO Baari VALUES (51, "Kallion B12", "Vaasankatu 12", 4, "ma-to 09-02, pe-la 08-04, su 09-02", null);
INSERT INTO Baari VALUES (52, "William K.", "Helsinginkatu 2" ,1,  "ma-ti 12-02, ke 12-4.30, to 12-02, pe-la 12-4.30, su 12-02", null);
INSERT INTO Baari VALUES (53, "Fairytale", "Helsinginkatu 7" , 1,  "ma-pe 16-02, la-su 14-02", null);
INSERT INTO Baari VALUES (54, "Kuikka", "Helsinginkatu 32", 1,  "ma-ti 16-24, 16-02, to 14-22. pe 16-02, la 1-02, su 14-22", null);
INSERT INTO Baari VALUES (55, "Luckynine", "Aleksis Kiven katu 30", 4,  "ma-su 12-02", null);
INSERT INTO Baari VALUES (56, "Bar Lab", "Alppikatu 17", 3,  "ma-su 09-02", null);
INSERT INTO Baari VALUES (57, "Brewster Bar", "Kaarlenkatu 1", 3,  "ma-to 15-04, pe-su 12-04", null);
INSERT INTO Baari VALUES (58, "Bambu", "Hämeentie 19",2,  "ma-to 11-20, pe 11-02, la-su 13-02", null);
INSERT INTO Baari VALUES (59, "Ravintola Kurjenlento", "Fleminginkatu 11 B", 3,  "ma-su 11-01", null);
INSERT INTO Baari VALUES (60, "Viva la vida", "Vaasankatu 18", 4, "ma-su 14-02", null);

INSERT INTO Baari VALUES (61, "Aleksis K", "Aleksis Kiven katu 14", 4,  "___", null);
INSERT INTO Baari VALUES (62, "Cow", "Läntinen Brahenkatu 2",1,  "ma-ti 16-23, ke-to 16-00, pe-la 16-01", null);
INSERT INTO Baari VALUES (63, "Cafe efes Bar", "Helsinginkatu 13", 1,  "ma-to 15-02, pe-la 15-4.30, su 15-02", null);
INSERT INTO Baari VALUES (64, "Tenho Restobar", "Helsinginkatu 15", 1,  "ma-to 16-24, pe 16-02, la 14-02, su 14-24", null);
INSERT INTO Baari VALUES (65, "Panema", "Helsinginkatu 11", 1,  "ma-ti 16-24, ke-to 16-02, pe-la 14-03, su 14-23", null);
INSERT INTO Baari VALUES (67, "Hesari 13", "Helsinginkatu 13", 1,  "ma-ti 09-00, ke-la 09-02", null);
INSERT INTO Baari VALUES (68, "Bar Tappen", "Kustaankatu 7", 4,  "ma-su 16-04", null);
INSERT INTO Baari VALUES (69, "Vivian's Kitchen & Bar", "Kustaankatu 4", 4,  "ma-pe 10.30-4.30, la-su 15-04.30", null);
INSERT INTO Baari VALUES (70, "RIVIERA", "Harjukatu 2", 4,  "ma-to 17-22, pe-la 17-02, su 16-21", null);

INSERT INTO Baari VALUES (71, "Tsemppi-Pub", "Kirstinkatu 13", 4,  "ma-su 9-19", null);
INSERT INTO Baari VALUES (73, "Salpimienta", "Fleminginkatu 7", 3,  "ma-su 10-02", null);
INSERT INTO Baari VALUES (74, "Las Vegas", "Viides linja 8", 2,  "ma-su 11-02", null);
INSERT INTO Baari VALUES (75, "Helsing Bar", "Kaarlenkatu 3-5", 3,  "ma-su 15-02", null);
INSERT INTO Baari VALUES (76, "Femma IV", "Viides linja 4", 2,  "ma-su 09-02", null);
INSERT INTO Baari VALUES (77, "Street bar", "Hämeentie 10", 3,  "ma-to 10.30-02, pe-la 10.30-4.30, su 12-00", "arkisin 17-22");
INSERT INTO Baari VALUES (78, "Mascot", "Neljäs linja 2", 2,  "ma-ti 15-02, ke-to 15-03, pe-la 15-04, su 15-02", null);
INSERT INTO Baari VALUES (79, "Katmando", "Porthaninkatu 9", 2,  "ti-su 15-02", null);
INSERT INTO Baari VALUES (80, "Stindepinde", "Vaasankatu 18", "ma-su 09-02", 4, "9-10, 18-19 sekä arkisin 22-23", null);



INSERT INTO Juomavalikoima VALUES (3.50, 4.50, 4.00, 4.00, 4.00, 1); -- done
INSERT INTO Juomavalikoima VALUES (3.80, 4.00, 5.50, 5.50, 4.50, 2); -- done
INSERT INTO Juomavalikoima VALUES (4.50, 5.00, 5.50, 5.50, 3.50, 3); -- done
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 4);
INSERT INTO Juomavalikoima VALUES (5.50, 4.20, 4.00, 6.50, 5.00, 5); -- done
INSERT INTO Juomavalikoima VALUES (4.90, 4.50, 5.70, 5.80, 00, 6); -- done
INSERT INTO Juomavalikoima VALUES (5.00, 5.00, 5.50, 5.50, 3.50, 7); -- done
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 8);
INSERT INTO Juomavalikoima VALUES (4.00, 8.00, 5.00, 5.00, 4.00, 9); -- done
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 10);

INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 11);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 12);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 13);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 14);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 16);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 17);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 18);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 19);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 20);

INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 21);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 22);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 23);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 24);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 25);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 26);
INSERT INTO Juomavalikoima VALUES (4.40, 3.60, 6.40, 7.00, 4.50, 27); -- done
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 28);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 29);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 30);

INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 31);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 32);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 34);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 35);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 36);
INSERT INTO Juomavalikoima VALUES (4.00, 8.00, 5.00, 5.00, 4.50, 37); -- done
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 38);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 39);
INSERT INTO Juomavalikoima VALUES (5.90, 5.50, 5.90, 6.00, 4.50, 40);

INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 41);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 42);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 43);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 44);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 45);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 46);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 47);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 48);
INSERT INTO Juomavalikoima VALUES (6.50, 5.00, 6.00, 6.00, 5.70, 50); -- done

INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 51);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 52);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 53);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 54);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 55);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 56);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 57);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 58);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 59);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 60);

INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 61);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 62);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 63);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 64);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 65);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 66);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 67);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 68);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 69);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 70);

INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 71);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 73);
INSERT INTO Juomavalikoima VALUES (4.20, 4.00, 5.50, 5.50, 4.50, 74); -- done
INSERT INTO Juomavalikoima VALUES (3.50, 5.00, 5.00, 4.50, 4.00, 75);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 76);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 77);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 78);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 79);
INSERT INTO Juomavalikoima VALUES (3.60, 00, 4.50, 4.50, 4.50, 80);


-- bilis, keittiö, lautapelit, jukeboxi, karaoke
INSERT INTO Varustelu VALUES (false, false, false, false, false, 1);
INSERT INTO Varustelu VALUES (false, false, false, false, false, 2);
INSERT INTO Varustelu VALUES (false, false, false, false, false, 3);
INSERT INTO Varustelu VALUES (false, true, true, false, false, false, 4); -- done
INSERT INTO Varustelu VALUES (false, true, false, false, false, 5); -- done
INSERT INTO Varustelu VALUES (false, false, false, false, false,  6);
INSERT INTO Varustelu VALUES (false, false, true, false, false, 7);
INSERT INTO Varustelu VALUES (false, false, true, false, false, 8);
INSERT INTO Varustelu VALUES (false, false, false, false,false,  9);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 10); -- done

INSERT INTO Varustelu VALUES (false, false, false, false, false, 11);
INSERT INTO Varustelu VALUES (false, false, false, false, true, 12); -- done
INSERT INTO Varustelu VALUES (false, false, false, false, false, 13);
INSERT INTO Varustelu VALUES (false, false, false, true, false, 14); -- done
INSERT INTO Varustelu VALUES (false, false, false, false,false, 16);
INSERT INTO Varustelu VALUES (false, false, false, false, false,17);
INSERT INTO Varustelu VALUES (false, true, true, false, false,18);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 19);
INSERT INTO Varustelu VALUES (true, false, false, false, false, 20);

INSERT INTO Varustelu VALUES (false, true, false, false, false, 21);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 22);
INSERT INTO Varustelu VALUES (false, false, false, false, false, 23);
INSERT INTO Varustelu VALUES (true, false, true, false, false, 24); -- done
INSERT INTO Varustelu VALUES (false, false, true, false, false, 25);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 26);
INSERT INTO Varustelu VALUES (false, true, true, false, false, 27); --  done
INSERT INTO Varustelu VALUES (true, false, false, false, false, 28);
INSERT INTO Varustelu VALUES (false, false, false, false, false, 29);
INSERT INTO Varustelu VALUES (false, false, false, false, false, 30);

INSERT INTO Varustelu VALUES (false, true, false, false, false, 31);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 32);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 34);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 35);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 36);
INSERT INTO Varustelu VALUES (false, false, false, false, false, 37);
INSERT INTO Varustelu VALUES (false, false, false, false, false, 38);
INSERT INTO Varustelu VALUES (true, true, false, false, false, 39);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 40);

INSERT INTO Varustelu VALUES (false, false, false, false, false, 41);
INSERT INTO Varustelu VALUES (false, false, false, false, false, 42);
INSERT INTO Varustelu VALUES (false, false, false, false, false, 43);
INSERT INTO Varustelu VALUES (false, false, false, false, false,  44);
INSERT INTO Varustelu VALUES (false, true, false, false, false,  45);
INSERT INTO Varustelu VALUES (false, true, true, false, false, 46);
INSERT INTO Varustelu VALUES (false, false, false, false, false, 47);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 48);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 50); -- done

INSERT INTO Varustelu VALUES (false, false, false, false, false,51);
INSERT INTO Varustelu VALUES (false, true, true, false, false, 52);
INSERT INTO Varustelu VALUES (false, false, false, false, false, 53);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 54);
INSERT INTO Varustelu VALUES (true, false, true, true, false, 55);
INSERT INTO Varustelu VALUES (false, true, true, false, false,  56);
INSERT INTO Varustelu VALUES (false, false, false, false, false,  57);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 58);
INSERT INTO Varustelu VALUES (false, false, false, false, false, 59);
INSERT INTO Varustelu VALUES (false, false, false, false, false, 60);

INSERT INTO Varustelu VALUES (false, false, false, false, false, 61);
INSERT INTO Varustelu VALUES (false, true, true, false, false, 62);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 63);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 64);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 65);
INSERT INTO Varustelu VALUES (false, false, false, false, false, 66);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 67);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 68);
INSERT INTO Varustelu VALUES (true, true, true, false, false,  69);
INSERT INTO Varustelu VALUES (false, false, false, false, false, 70);

INSERT INTO Varustelu VALUES (false, false, false, false, false, 71);
INSERT INTO Varustelu VALUES (false, true, false, false, false, 73);
INSERT INTO Varustelu VALUES (false, false, false, true, false, 74); -- done
INSERT INTO Varustelu VALUES (false, false, false, false, false, 75);
INSERT INTO Varustelu VALUES (false, false, true, false, false, 76);
INSERT INTO Varustelu VALUES (true, true, false, false, false, 77);
INSERT INTO Varustelu VALUES (true, true, true, false, false, 78);
INSERT INTO Varustelu VALUES (false, false, false, false, false, 79);
INSERT INTO Varustelu VALUES (false, false, false, false, false, 80);

