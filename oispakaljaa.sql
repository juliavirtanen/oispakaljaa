DROP DATABASE IF EXISTS Oispakaljaa;
CREATE DATABASE Oispakaljaa;
USE Oispakaljaa;

CREATE TABLE Baari
(
  Id INT NOT NULL,
  Nimi VARCHAR(40) NOT NULL,
  Osoite VARCHAR(100)  NOT NULL,
  Aukioloaika INT NOT NULL,
  Alue INT NOT NULL,
  Happyhour INT,
  PRIMARY KEY (Id)
);

CREATE TABLE Juomavalikoima
(
  Kaljanhinta float NOT NULL,
  Viininhinta float NOT NULL,
  Siiderinhinta float NOT NULL,
  Lonkeronhinta float NOT NULL,
  Shotinhinta float NOT NULL,
  Id INT NOT NULL,
  FOREIGN KEY (Id) REFERENCES Baari(Id)
);

CREATE TABLE Varustelu
(
  Bilis BOOLEAN NOT NULL,
  Keittiö BOOLEAN NOT NULL,
  Lautapelit BOOLEAN NOT NULL,
  Jukeboxi BOOLEAN NOT NULL,
  Id INT NOT NULL,
  FOREIGN KEY (Id) REFERENCES Baari(Id)
);

INSERT INTO Baari VALUES (1, "Bar Kalliohovi", "Vaasankatu 17", 20, 1, null);
INSERT INTO Baari VALUES (2, "Iltakoulu", "Vaasankatu 5");
INSERT INTO Baari VALUES (3, "Solmu Pub", "Vaasankatu 8");
INSERT INTO Baari VALUES (4, "Olutravintola Hilpeä Hauki", "Vaasankatu 7");
INSERT INTO Baari VALUES (5, "Kustaa Vaasa", "Vaasankatu 10");
INSERT INTO Baari VALUES (6, "Pub Heinähattu", "Vaasankatu 23");
INSERT INTO Baari VALUES (7, "Bar Molotow", "Vaasankatu 29");
INSERT INTO Baari VALUES (8, "Kultapalmu", "Vaasankatu 8");
INSERT INTO Baari VALUES (9, "Siima Baari", "Vaasankatu 25");
INSERT INTO Baari VALUES (10, "On The Rocks", "Porvoonkatu 1");

INSERT INTO Baari VALUES (11, "Kaiku", "kaikukatu 4");
INSERT INTO Baari VALUES (12, "Musta Kissa", "Toinen linja 15");
INSERT INTO Baari VALUES (13, "Kipinä", "Kolmas linja 34");
INSERT INTO Baari VALUES (13, "5th Street Bar & Cafe", "Viides linja 7");
INSERT INTO Baari VALUES (14, "Majava Baari", "Porthaninkatu 9");
INSERT INTO Baari VALUES (15, "Kuudes linja", "Hämeentie 13 B");
INSERT INTO Baari VALUES (16, "Roskapankki", "Helsinginkatu 20");
INSERT INTO Baari VALUES (17, "Om'pu", "Siltasaarenkatu 15");
INSERT INTO Baari VALUES (18, "Toveri", "Castréninkatu 3");
INSERT INTO Baari VALUES (19, "Bar Loosister", "Hämeentie 50");
INSERT INTO Baari VALUES (20, "Siltanen", "Hämeentie 13 B");

INSERT INTO Baari VALUES (21, "Mäkikupla", "Torkkelinkatu 2 B");
INSERT INTO Baari VALUES (22, "Tenkka", "Helsinginkatu 15");
INSERT INTO Baari VALUES (23, "Sirdie", "Kolmas linja 21");
INSERT INTO Baari VALUES (24, "Kombo", "Kulmavuorenkatu 2");
INSERT INTO Baari VALUES (25, "Pub Porthan", "Porthaninkatu 10");
INSERT INTO Baari VALUES (26, "Wino", "Fleminginkatu 11");
INSERT INTO Baari VALUES (27, "Sivukirjasto", "Fleminginkatu 5");
INSERT INTO Baari VALUES (28, "Bar Lepakkomies", "Helsinginkatu 1");
INSERT INTO Baari VALUES (29, "Pulmu", "Fleminginkatu 13");
INSERT INTO Baari VALUES (30, "Bar Mucava", "Hämeentie 30");

INSERT INTO Baari VALUES (31, "Bar Bronco", "Hämeentie 23");
INSERT INTO Baari VALUES (32, "Kolme Kaisaa", "Hämeentie 29");
INSERT INTO Baari VALUES (33, "Abin Baari", "Fleminginkatu 13");
INSERT INTO Baari VALUES (34, "Rytmi", "Toinen linja 2");
INSERT INTO Baari VALUES (35, "Loung3", "Kolmas linja 18");
INSERT INTO Baari VALUES (36, "Cafe Talo", "Hämeentie 2");
INSERT INTO Baari VALUES (37, "Bar 21", "Helsinginkatu 21");
INSERT INTO Baari VALUES (38, "Arizona", "Castréninkatu 7");
INSERT INTO Baari VALUES (39, "Stadin Tähti", "Mäkenlänkatu 2");
INSERT INTO Baari VALUES (40, "Cella", "Fleminginkatu 15");

INSERT INTO Baari VALUES (41, "Relaxin", "Helsinginkatiu 13");
INSERT INTO Baari VALUES (42, "Kallion pörssi", "Alppikatu 17");
INSERT INTO Baari VALUES (43, "Pressapari", "Wallininkatu 10");
INSERT INTO Baari VALUES (44, "Kurvitar", "Hämeentie 58-60");
INSERT INTO Baari VALUES (45, "TerassiBAR", "Porthaninkatu 9");
INSERT INTO Baari VALUES (46, "Kallion Seurahuone", "Fleminginkatu 6");
INSERT INTO Baari VALUES (47, "Populus", "Aleksis Kiven katu 22");
INSERT INTO Baari VALUES (48, "Kallion Mestari", "Kustaankatu 4 A");
INSERT INTO Baari VALUES (49, "Kola", "Helsinginkatu 13");
INSERT INTO Baari VALUES (50, "Ravintola Oiva", "Porthaninkatu 5");

INSERT INTO Baari VALUES (51, "Kallion B12", "Vaasankatu 12");
INSERT INTO Baari VALUES (52, "William K.", "Helsinginkatu 2");
INSERT INTO Baari VALUES (53, "Fairytale", "Helsinginkatu 7");
INSERT INTO Baari VALUES (54, "Kuikka", "Helsinginkatu 32");
INSERT INTO Baari VALUES (55, "Luckynine", "Aleksis Kiven katu 30");
INSERT INTO Baari VALUES (56, "Bar Lab", "Alppikatu 17");
INSERT INTO Baari VALUES (57, "Brewster Bar", "Kaarlenkatu 1");
INSERT INTO Baari VALUES (58, "Bambu", "Hämeentie 19");
INSERT INTO Baari VALUES (59, "Ravintola Kurjenlento", "Fleminginkatu 11 B");
INSERT INTO Baari VALUES (60, "Viva la vida", "Vaasankatu 18");

INSERT INTO Baari VALUES (61, "Aleksis K", "Aleksis Kiven katu 14");
INSERT INTO Baari VALUES (62, "Parnell's", "Läntinen Brahenkatu 2");
INSERT INTO Baari VALUES (63, "Cafe efes Bar", "Helsinginkatu 13");
INSERT INTO Baari VALUES (64, "Tenho Restobar", "Helsinginkatu 15");
INSERT INTO Baari VALUES (65, "Panema", "Helsinginkatu 11");
INSERT INTO Baari VALUES (66, "Bar Cow", "Läntinen Brahenkatu 2");
INSERT INTO Baari VALUES (67, "Hesari 13", "Helsinginkatu 13");
INSERT INTO Baari VALUES (68, "Bar Tappen", "Kustaankatu 7");
INSERT INTO Baari VALUES (69, "Vivian's Kitchen & Bar", "Kustaankatu 4");
INSERT INTO Baari VALUES (70, "RIVIERA", "Harjukatu 2");

INSERT INTO Baari VALUES (71, "Tsemppi-Pub", "Kirstinkatu 13");
INSERT INTO Baari VALUES (72, "Pub Porthan", "Porthaninkatu 10");
INSERT INTO Baari VALUES (73, "Salpimienta", "Fleminginkatu 7");
INSERT INTO Baari VALUES (74, "Las Vegas", "Viides linja 8");
INSERT INTO Baari VALUES (75, "Helsing Bar", "Kaarlenkatu 3-5");
INSERT INTO Baari VALUES (76, "Femma IV", "Viides linja 4");
INSERT INTO Baari VALUES (77, "Street bar", "Hämeentie 10");
INSERT INTO Baari VALUES (78, "Mascot", "Neljäs linja 2");
INSERT INTO Baari VALUES (79, "Katmando", "Porthaninkatu 9");



INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 1);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 2);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 3);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 4);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 5);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 6);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 7);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 8);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 9);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 10);

INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 11);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 12);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 13);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 14);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 15);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 16);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 17);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 18);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 19);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 20);

INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 21);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 22);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 23);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 24);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 25);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 26);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 27);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 28);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 29);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 30);

INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 31);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 32);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 33);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 34);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 35);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 36);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 37);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 38);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 39);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 40);

INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 41);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 42);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 43);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 44);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 45);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 46);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 47);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 48);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 49);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 50);

INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 51);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 52);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 53);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 54);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 55);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 56);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 57);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 58);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 59);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 60);

INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 61);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 62);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 63);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 64);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 65);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 66);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 67);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 68);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 69);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 70);

INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 71);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 72);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 73);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 74);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 75);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 76);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 77);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 78);
INSERT INTO Juomavalikoima VALUES (3.50, 8.00, 4.00, 4.00, 3.50, 79);


bilis, keittiö, lautapelit, jukeboxi
INSERT INTO Varustelu VALUES (false, false, false, false, 1);
INSERT INTO Varustelu VALUES (false, false, false, false, 2);
INSERT INTO Varustelu VALUES (false, false, false, false, 3);
INSERT INTO Varustelu VALUES (false, false, false, false, 4);
INSERT INTO Varustelu VALUES (false, true, false, false, 5);
INSERT INTO Varustelu VALUES (false, false, false, false, 6);
INSERT INTO Varustelu VALUES (false, false, true, false, 7);
INSERT INTO Varustelu VALUES (false, false, false, false, 8);
INSERT INTO Varustelu VALUES (false, false, false, false, 9);
INSERT INTO Varustelu VALUES (false, false, false, false, 10);

INSERT INTO Varustelu VALUES (false, false, false, false, 11);
INSERT INTO Varustelu VALUES (false, false, false, false, 12);
INSERT INTO Varustelu VALUES (false, false, false, false, 13);
INSERT INTO Varustelu VALUES (false, false, false, false, 14);
INSERT INTO Varustelu VALUES (false, false, false, false, 15);
INSERT INTO Varustelu VALUES (false, false, false, false, 16);
INSERT INTO Varustelu VALUES (false, false, false, false, 17);
INSERT INTO Varustelu VALUES (false, false, true, false, 18);
INSERT INTO Varustelu VALUES (false, true, false, false, 19);
INSERT INTO Varustelu VALUES (false, false, false, false, 20);

INSERT INTO Varustelu VALUES (false, true, false, false, 21);
INSERT INTO Varustelu VALUES (false, true, false, false, 22);
INSERT INTO Varustelu VALUES (false, false, false, false, 23);
INSERT INTO Varustelu VALUES (false, false, false, false, 24);
INSERT INTO Varustelu VALUES (false, false, false, false, 25);
INSERT INTO Varustelu VALUES (false, true, false, false, 26);
INSERT INTO Varustelu VALUES (false, true, true, false, 27);
INSERT INTO Varustelu VALUES (true, false, false, false, 28);
INSERT INTO Varustelu VALUES (false, false, false, false, 29);
INSERT INTO Varustelu VALUES (false, false, false, false, 30)

INSERT INTO Varustelu VALUES (false, true, false, false, 31);
INSERT INTO Varustelu VALUES (false, false, false, false, 32);
INSERT INTO Varustelu VALUES (false, false, false, false,33);
INSERT INTO Varustelu VALUES (false, false, false, false, 34);
INSERT INTO Varustelu VALUES (false, false, false, false, 35);
INSERT INTO Varustelu VALUES (false, false, false, false, 36);
INSERT INTO Varustelu VALUES (false, false, false, false, 37);
INSERT INTO Varustelu VALUES (false, false, false, false, 38);
INSERT INTO Varustelu VALUES (true, false, false, false, 39);
INSERT INTO Varustelu VALUES (false, true, false, false, 40);

INSERT INTO Varustelu VALUES (false, false, false, false, 41);
INSERT INTO Varustelu VALUES (false, false, false, false, 42);
INSERT INTO Varustelu VALUES (false, false, false, false, 43);
INSERT INTO Varustelu VALUES (false, false, false, false, 44);
INSERT INTO Varustelu VALUES (false, false, false, false, 45);
INSERT INTO Varustelu VALUES (false, false, true, false, 46);
INSERT INTO Varustelu VALUES (false, false, false, false, 47);
INSERT INTO Varustelu VALUES (false, false, false, false, 48);
INSERT INTO Varustelu VALUES (false, false, false, false, 49);
INSERT INTO Varustelu VALUES (false, true, false, false, 50);

INSERT INTO Varustelu VALUES (false, false, false, false, 51);
INSERT INTO Varustelu VALUES (false, false, false, false, 52);
INSERT INTO Varustelu VALUES (false, false, false, false, 53);
INSERT INTO Varustelu VALUES (false, false, false, false, 54);
INSERT INTO Varustelu VALUES (false, false, false, false, 55);
INSERT INTO Varustelu VALUES (false, false, false, false, 56);
INSERT INTO Varustelu VALUES (false, false, false, false, 57);
INSERT INTO Varustelu VALUES (false, false, false, false, 58);
INSERT INTO Varustelu VALUES (false, false, false, false, 59);
INSERT INTO Varustelu VALUES (false, false, false, false, 60);

INSERT INTO Varustelu VALUES (false, false, false, false, 61);
INSERT INTO Varustelu VALUES (false, false, false, false, 62);
INSERT INTO Varustelu VALUES (false, false, false, false, 63);
INSERT INTO Varustelu VALUES (false, true, false, false, 64);
INSERT INTO Varustelu VALUES (false, false, false, false, 65);
INSERT INTO Varustelu VALUES (false, false, false, false, 66);
INSERT INTO Varustelu VALUES (false, false, false, false, 67);
INSERT INTO Varustelu VALUES (false, false, false, false, 68);
INSERT INTO Varustelu VALUES (false, true, false, false, 69);
INSERT INTO Varustelu VALUES (false, false, false, false, 70);

INSERT INTO Varustelu VALUES (false, false, false, false, 71);
INSERT INTO Varustelu VALUES (false, false, false, false, 72);
INSERT INTO Varustelu VALUES (false, false, false, false, 73);
INSERT INTO Varustelu VALUES (false, false, false, true, 74);
INSERT INTO Varustelu VALUES (false, false, false, false, 75);
INSERT INTO Varustelu VALUES (false, false, false, false, 76);
INSERT INTO Varustelu VALUES (false, false, false, false, 77);
INSERT INTO Varustelu VALUES (true, false, true, false, 78);
INSERT INTO Varustelu VALUES (false, false, false, false, 79);
INSERT INTO Varustelu VALUES (false, false, false, false, 80);

