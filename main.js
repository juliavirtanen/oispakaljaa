var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
var url = "getKalja.php";

output.innerHTML = slider.value + "€"; // Display the default slider value

slider.oninput = function() {
    output.innerHTML = this.value + "€";
};

let areas = [];

// Kartan klikkifunktiot
function hesari() { // //alue 1
    if (areas.includes(1)) { // Jos klikattu alue on jo aktivoitu hakuun
        for (let i = 0; i < areas.length; i++) {
            if(areas[i] == 1) {
                areas.splice(i, 1); // poistetaan alue hausta
            }
        }
    } else {
        areas.push(1);
    }
    naytaAlueet();
    console.log('hesari');
    console.log(areas);
};

function linjat() { // //alue 2
    if (areas.includes(2)) { // Jos klikattu alue on jo aktivoitu hakuun
        for (let i = 0; i < areas.length; i++) {
            if(areas[i] == 2) {
                areas.splice(i, 1); // poistetaan alue hausta
            }
        }
    } else {
        areas.push(2);
    }
    naytaAlueet();

    console.log('linja');
    console.log(areas);
};

function karhupuisto() { // //alue 3
    if (areas.includes(3)) { // Jos klikattu alue on jo aktivoitu hakuun
        for (let i = 0; i < areas.length; i++) {
            if(areas[i] == 3) {
                areas.splice(i, 1); // poistetaan alue hausta
            }
        }
    } else {
        areas.push(3);
    }
    naytaAlueet();

    console.log('karhupuisto');
    console.log(areas);
};

function harju() { // // alue 4
    if (areas.includes(4)) { // Jos klikattu alue on jo aktivoitu hakuun
        for (let i = 0; i < areas.length; i++) {
            if(areas[i] == 4) {
                areas.splice(i, 1); // poistetaan alue hausta
            }
        }
    } else {
        areas.push(4);
    }
    naytaAlueet();

    console.log('harju');
    console.log(areas);
};

function naytaAlueet() {
    let teksti = "";
    for (let i = 0; i < areas.length; i++) {
        switch (areas[i]) {
            case 1:
                teksti += "Hesari, ";
                break;
            case 2:
                teksti += "Linjat, ";
                break;
            case 3:
                teksti += "Karhupuisto, ";
                break;
            case 4:
                teksti += "Harju, ";
                break;
        }
    }
    document.getElementById('alueteksti').innerHTML = teksti;

}

/*
var baari1 = {
    nimi: "Kallionhovi",
    osoite: "Kuja 3",
    aukioloaika: "La-Su 15-01",
    tuotteet: {"Kalja": 3.5,
    "Lonkero": 4,
    "Viini": 6}

};

var baari2 = {
    nimi: "Las Palmas",
    osoite: "Vaasankatu 6",
    aukioloaika: "Ma-Su 14-04",
    tuotteet: {"Kalja": 4.5,
        "Lonkero": 3,
        "Viini": 7}
};

var baarit = [baari1, baari2];
*/

function makeRequest() {
    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
        httpRequest = new XMLHttpRequest();
    } else if (window.ActiveXObject) { // IE
        try {
            httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
            }
        }
    }

    if (!httpRequest) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }

    // set a callback function for when the httpRequest completes
    httpRequest.onreadystatechange = getResults;


    // CHECKBOKSIT
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    var checkedOne = Array.prototype.slice.call(checkboxes).some(x => x.checked);

    // Go through all the checkboxes and save the checked id's and values into a table.
    let tuotteet = ["Kalja", "Viini", "Siideri", "Lonkero", "Shotti"];
    let palvelut = ["Bilis", "Lautapelit", "Jukeboxi", "Karaoke", "Keittio"]
    let kiinnostavatTuotteet = {};
    let kiinnostavatPalvelut = {};

    for (let i = 0; i < tuotteet.length; i++) {
        let input = document.getElementById(tuotteet[i]);
        if (input.checked === true) {
            kiinnostavatTuotteet[tuotteet[i]] = slider.value; // Jos checkbox on chekattu, lisätään se tuotelistaobjektiin {tuote:hinta}
        }
    }

    for (let i = 0; i < palvelut.length; i++) {
        let input = document.getElementById(palvelut[i]);
        if (input.checked === true) kiinnostavatPalvelut[palvelut[i]] = true; // Jos checkbox on chekattu, lisätään se kiinnostavien palvelujen arrayhin nimellään
    }

    console.log(kiinnostavatTuotteet);
    console.log(kiinnostavatPalvelut);
    // Tässä vaiheessa chekatut tuotteet objektissa kiinnostavatTuotteet {tuote1:hinta, tuote2:hinta...}
    // Chekatut palvelut objektissa kiinnostavatPalvelut {palvelu1:true,palvelu2:true...}

    let query = url + "?";
    for (let key in kiinnostavatTuotteet) {
        if (Object.keys(kiinnostavatTuotteet).indexOf(key) == 0){
            query += key + "=" + kiinnostavatTuotteet[key];
        } else {
            query += "&" + key + "=" + kiinnostavatTuotteet[key];
        }

    }

    for (let key in kiinnostavatPalvelut) {
        if (Object.keys(kiinnostavatPalvelut).indexOf(key) == 0 && Object.keys(kiinnostavatTuotteet).length <= 0) { // Jos tuotteita on 0, aloitetaan tästä
            query += key + "=" + kiinnostavatPalvelut[key];
        } else {
            query += "&" + key + "=" + kiinnostavatPalvelut[key];
        }
    }

    // Alue mukaan queryyn
    if (areas.length > 0) {
        if (kiinnostavatPalvelut.length <= 0 >= kiinnostavatTuotteet) { // Jos query alkaa alueella siinä ei saa oll &-merkkiä alussa
            for (let i = 0; i < areas.length; i++) {
                if (i == 0) { // Jos ollaan alue-listan ekassa alkiossa, aloitetaan query rakentaminen ilman &-merkkiä.
                    // Huom! Lähetämme GET-metodilla arrayn Alue = [a, b, c] tekemällä queryn "Alue[] = a, Alue[] = b, Alue[] = c"
                    query += "Alue[]=" + areas[i];
                }
                // Muissa kuin ekassa alkiossa täytyy lisätä &-merkki queryyn
                query += "&Alue[]=" + areas[i];
            }
        } else { // Jos queryssä on jo tuotteita ja/tai palveluita, jatketaan queryä &-merkillä
            for (let i = 0; i < areas.length; i++) {
                query += "&Alue[]=" + areas[i];
            }
        }
    }

    let resultsNode = document.getElementById('results');
    while (resultsNode.lastChild) {
        resultsNode.removeChild(resultsNode.lastChild);
    }
    console.log(query);

    httpRequest.open('GET', query, true);
    httpRequest.send();

    function getResults() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                //alert(httpRequest.responseText);
                if (httpRequest.responseText != "") {
                    let baarit = JSON.parse(httpRequest.responseText); // Taulukko: alkio0 = 1. baari, alkio1 = 2. baari
                    printtaaTulos(baarit);
                    //alert(httpRequest.responseText);
                } else {
                    alert("Hakuehdoillasi ei löytynyt yhtään baaria :(");
                }

            } else {
                alert(httpRequest.status);

            }
        } else {
            console.log(httpRequest.readyState);
        }
    }

};
